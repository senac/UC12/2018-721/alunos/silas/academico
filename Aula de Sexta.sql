CREATE TABLE IF NOT EXISTS DICIPLINAS ( 
ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY  , 
UC varchar(5)  NOT NULL ,
DICIPLINA varchar(200) not null
) ;

insert into DICIPLINAS(UC, DICIPLINA) VALUES ('UC1' , 'Planejar e executar a montagem de computadores');
insert into DICIPLINAS(UC, DICIPLINA) VALUES ('UC2' , 'Planejar e executar a instalação de hardware e software para computadores');
insert into DICIPLINAS(UC, DICIPLINA) VALUES ('UC3' , 'Planejar e executar a manutenção de computadores');
insert into DICIPLINAS(UC, DICIPLINA) VALUES ('UC4' , 'Planejar e executar computadores');
insert into DICIPLINAS(UC, DICIPLINA) VALUES ('UC5' , 'Planejar e executar a instalação de redes locais de computadores');
insert into DICIPLINAS(UC, DICIPLINA) VALUES ('UC6' , 'Planejar e executar a manutenção de redes locais de computadores');
insert into DICIPLINAS(UC, DICIPLINA) VALUES ('UC7' , 'Planejar e executar a instalação, a configuração e o monitoramento de sistemas operacionais de redes locais (servidores)');
insert into DICIPLINAS(UC, DICIPLINA) VALUES ('UC8', 'Projeto Integrador Assistente de Operação de Redes de Computadores');
insert into DICIPLINAS(UC, DICIPLINA) VALUES ('UC9' , 'Conceber, analisar e planejar o desenvolvimento de software');
insert into DICIPLINAS(UC, DICIPLINA) VALUES ('UC10' , 'Executar os processos de codificação, manutenção e documentação de aplicativos computacionais para desktops');
insert into DICIPLINAS(UC, DICIPLINA) VALUES ('UC11' , 'Executar os processos de codificação, manutenção e documentação de aplicativos computacionais para dispositivos móveis');
insert into DICIPLINAS(UC, DICIPLINA) VALUES ('UC12' , 'Executar os processos de codificação, manutenção e documentação de aplicativos computacionais para internet');
insert into DICIPLINAS(UC, DICIPLINA) VALUES ('UC13' , 'Executar teste e implantação de aplicativos computacionais');
insert into DICIPLINAS(UC, DICIPLINA) VALUES ('UC14' , 'Desenvolver e organizar elementos estruturais de sites');
insert into DICIPLINAS(UC, DICIPLINA) VALUES ('UC15' , 'Manipular e otimizar imagens vetoriais, bitmaps gráficos e elementos visuais de navegação para web');
insert into DICIPLINAS(UC, DICIPLINA) VALUES ('UC16' , 'Projeto Integrador Assistente de Desenvolvimento de Aplicativos Computacionais ');

select * from diciplinas;


drop table diciplinas;
--  --
CREATE TABLE IF NOT EXISTS PROFESSORES ( 
ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY  , 
UC varchar(5) not null ,
PROFESSOR varchar(200)  NOT NULL 
) ;

insert into PROFESSORES(UC, PROFESSOR) VALUES ('UC1' , 'Thiago');
insert into PROFESSORES(UC, PROFESSOR) VALUES ('UC2' , 'Thiago');
insert into PROFESSORES(UC, PROFESSOR) VALUES ('UC3' , 'Thiago');
insert into PROFESSORES(UC, PROFESSOR) VALUES ('UC4' , 'Thiago');
insert into PROFESSORES(UC, PROFESSOR) VALUES ('UC5' , 'Thiago');
insert into PROFESSORES(UC, PROFESSOR) VALUES ('UC6' , 'Jose');
insert into PROFESSORES(UC, PROFESSOR) VALUES ('UC7' , 'Jose');
insert into PROFESSORES(UC, PROFESSOR) VALUES ('UC8' , 'Jose');
insert into PROFESSORES(UC, PROFESSOR) VALUES ('UC9' , 'Daniel');
insert into PROFESSORES(UC, PROFESSOR) VALUES ('UC10' , 'Daniel');
insert into PROFESSORES(UC, PROFESSOR) VALUES ('UC11' , 'Daniel');
insert into PROFESSORES(UC, PROFESSOR) VALUES ('UC12' , 'Daniel');
insert into PROFESSORES(UC, PROFESSOR) VALUES ('UC13' , 'Daniel');
insert into PROFESSORES(UC, PROFESSOR) VALUES ('UC14' , 'Daniel');
insert into PROFESSORES(UC, PROFESSOR) VALUES ('UC15' , 'Marcelo');
insert into PROFESSORES(UC, PROFESSOR) VALUES ('UC16' , 'Daniel');

select * from PROFESSORES;

 
drop table professores;
--  --
CREATE TABLE IF NOT EXISTS PROFESSOR_DICIPLINAS ( 
ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY  , 
PROFESSOR varchar(200) not null , 
DICIPLINA varchar(200)  NOT NULL 
) ;

INSERT INTO Professor_Diciplinas(ID_PROFESSOR , ID_DICIPLINA ) VALUES ( 3, 1 ) ; 
INSERT INTO Professor_Diciplinas(ID_PROFESSOR , ID_DICIPLINA ) VALUES ( 3, 2 ) ; 
INSERT INTO Professor_Diciplinas(ID_PROFESSOR , ID_DICIPLINA ) VALUES ( 3, 3) ; 
INSERT INTO Professor_Diciplinas(ID_PROFESSOR , ID_DICIPLINA ) VALUES ( 5, 4) ; 
INSERT INTO Professor_Diciplinas(ID_PROFESSOR , ID_DICIPLINA ) VALUES ( 4, 5) ; 
INSERT INTO Professor_Diciplinas(ID_PROFESSOR , ID_DICIPLINA ) VALUES ( 2, 6) ; 
INSERT INTO Professor_Diciplinas(ID_PROFESSOR , ID_DICIPLINA ) VALUES ( 2, 7) ; 
INSERT INTO Professor_Diciplinas(ID_PROFESSOR , ID_DICIPLINA ) VALUES ( 2, 8) ; 
INSERT INTO Professor_Diciplinas(ID_PROFESSOR , ID_DICIPLINA ) VALUES ( 2, 9) ; 
INSERT INTO Professor_Diciplinas(ID_PROFESSOR , ID_DICIPLINA ) VALUES ( 1, 10) ; 
INSERT INTO Professor_Diciplinas(ID_PROFESSOR , ID_DICIPLINA ) VALUES ( 1, 11) ; 
INSERT INTO Professor_Diciplinas(ID_PROFESSOR , ID_DICIPLINA ) VALUES ( 1, 12) ; 
INSERT INTO Professor_Diciplinas(ID_PROFESSOR , ID_DICIPLINA ) VALUES ( 1, 13) ; 
INSERT INTO Professor_Diciplinas(ID_PROFESSOR , ID_DICIPLINA ) VALUES ( 6, 14) ; 
INSERT INTO Professor_Diciplinas(ID_PROFESSOR , ID_DICIPLINA ) VALUES ( 1, 15) ; 
INSERT INTO Professor_Diciplinas(ID_PROFESSOR , ID_DICIPLINA ) VALUES ( 1, 16) ; 


drop table professor_diciplinas;

select * from professores inner join  professor_diciplina 
on professor.ID = professor_disciplina.ID_PROFESSOR ;
-- --

 -- --
 -- 2 Ordem Inversa --
 select * from diciplinas ORDER BY 1 DESC;
 -- desc ou asc --
 -- order by nome da colona --
 -- 3 Lista qual professor administra --
 select * from PROFESSORES where UC= 5;
 -- 
 
select p.professores as professor, count(1) as 'UCS administradas' from professor p
inner join professor_diciplinas pd on p.id = pd.ID_PROFESSOR
inner join diciplinas d on pd.ID_DICIPLINA = d.ID
